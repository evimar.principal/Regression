# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 17:05:59 2017

@author: evimar.principal@gmail.com
"""

import sys
import numpy as np

lamba, sigma2, x_file, y_file, t_file = sys.argv[1:]

lamba, sigma2 = float(lamba), float(sigma2)

X = np.genfromtxt(x_file, delimiter=',')
y = np.genfromtxt(y_file, delimiter=',')
test = np.genfromtxt(t_file, delimiter=',')

n, d = X.shape

i = lamba*np.identity(d)+X.transpose().dot(X)
j = X.transpose().dot(y)

w = np.linalg.solve(i,j) 

nombre = "wRR_"+str(int(lamba))+".csv"
print("W_RR =",w)
l = w.tolist()
with open(nombre, mode='wt') as f:
    f.write('\n'.join(str(line) for line in l))
 
#Active Learning
#if test dont has one then we will remove the last dimention
        
nombre2 = "active_"+str(int(lamba))+"_"+str(int(sigma2))+".csv"

#calcular SIGMA

SIGMAinverse = lamba * np.eye(d) + (1/sigma2)*X.transpose().dot(X)
SIGMA = np.linalg.inv(SIGMAinverse)

ids = [i for i in range(test.shape[0])]

testID =  np.insert(test, 0, ids, axis=1)
picked = []
for i in range(10):
    #calcular la matrix de covarianzas
    covarianzas = np.array([sigma2 + x.transpose().dot(SIGMA).dot(x) for x in testID[:,1:]])

    #First pick
    pickpoint = np.argmax(covarianzas)
    picked.append(int(testID[pickpoint,0]))  #column 0 is the index
    xnaug = testID[pickpoint,1:]    
                  
    #update testID remove the pick point
    testID = np.delete(testID, pickpoint, axis=0)
    
    #update SIGMA
    SIGMAinverse = SIGMAinverse + (1/sigma2)*xnaug.dot(xnaug.transpose())
    SIGMA = np.linalg.inv(SIGMAinverse)
    

picktxt = str(picked)[1:-1].replace(" ","")

print("active=",picktxt)
with open(nombre2, mode='wt') as f:
    f.write(picktxt)
